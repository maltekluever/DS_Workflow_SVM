# Prerequisites / Required Packages
- sklearn
- matplotlib
- numpy
- pandas

# Presentation of the Chapter "Support Vector Machines"
### Presentation.pdf
holds the slides of our presentation
### BookChapter.ipynb
holds the jupyter notebook for the book chapter examples
# Own Example for Support Vector Machines
### Example.ipynb
holds the jupyter notebook for our own example
### data.csv
holds the ANSUR II dataset which includes anthropometric measures of 6,000 US soldiers from 2012.
